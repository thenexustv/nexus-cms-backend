package com.nexus.cms.backend.service;

import com.nexus.cms.backend.model.AlbumArt;
import com.nexus.cms.backend.model.People;
import com.nexus.cms.backend.repository.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleService {

    @Autowired
    private PeopleRepository peopleRepository;

    public List<People> getPeople() {
        return peopleRepository.findAll();
    }

    public Page<People> getPeople(Pageable pageable) {
        return peopleRepository.findAll(pageable);
    }

    public People getById(Integer id) {
        return peopleRepository.findOne(id);
    }

}
