package com.nexus.cms.backend.service;

import com.nexus.cms.backend.model.Series;
import com.nexus.cms.backend.repository.SeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeriesService {

    @Autowired
    private SeriesRepository seriesRepository;


    public List<Series> getSeries() {
        return seriesRepository.findAll();
    }

    public Page<Series> getSeries(Pageable pageable) {
        return seriesRepository.findAll(pageable);
    }

    public Series getById(Integer id) {
        return seriesRepository.findOne(id);
    }

    public Series getBySlug(String slug) {
        return seriesRepository.findBySlug(slug);
    }
}
