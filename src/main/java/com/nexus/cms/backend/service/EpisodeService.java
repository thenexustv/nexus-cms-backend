package com.nexus.cms.backend.service;

import java.util.List;

import com.nexus.cms.backend.model.Episode;
import com.nexus.cms.backend.repository.EpisodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EpisodeService {

    @Autowired
    private EpisodeRepository episodeRepository;

    public Episode getEpisodeById(Integer id) {
        return episodeRepository.findOne(id);
    }

    public List<Episode> getEpisodes() {
        return episodeRepository.findAll();
    }

    public Page<Episode> getEpisodes(Pageable pageable) {
        return episodeRepository.findAll(pageable);
    }
}
