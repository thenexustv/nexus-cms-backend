package com.nexus.cms.backend.service;

import com.nexus.cms.backend.model.AlbumArt;
import com.nexus.cms.backend.repository.AlbumArtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumArtService {

    @Autowired
    private AlbumArtRepository albumArtRepository;


    public List<AlbumArt> getAlbumArt() {
        return albumArtRepository.findAll();
    }

    public Page<AlbumArt> getAlbumArt(Pageable pageable) {
        return albumArtRepository.findAll(pageable);
    }

    public AlbumArt getById(Integer id) {
        return albumArtRepository.findOne(id);
    }

}
