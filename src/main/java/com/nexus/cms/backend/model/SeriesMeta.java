package com.nexus.cms.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "series_metas")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class SeriesMeta {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String landingContent;
    private String attributionContent;
    private String feedAuthor;
    private String feedGeographicLocation;
    private String feedEpisodeFrequency;
    private String feedImageUrl;
    private String feedTrackingUrl;
    private String googlePlaySubscriptionUrl;
    private String itunesSubscriptionUrl;
    private String itunesSummary;
    private String itunesKeywords;
    private String itunesCategoryOverall;
    private String itunesCategoryPrimary;
    private String itunesCategorySecondary;
    private String itunesEmail;
    private String itunesImageUrl;
    private Boolean itunesExplicit;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "series_id")
    private Series series;
}
