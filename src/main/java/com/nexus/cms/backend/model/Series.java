package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "series")
public class Series implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String name;
    private String slug;
    private String longSlug;
    private String description;
    private Boolean hidden;
    private Boolean retired;
    private Boolean hiatus;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    @OneToOne(mappedBy = "series")
    private SeriesMeta meta;

}
