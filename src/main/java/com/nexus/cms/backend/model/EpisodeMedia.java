package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "episodes_medias")
public class EpisodeMedia {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String type;
    private String mime;
    private String meta;
    private int length;
    private int size;
    private String url;
    private byte hidden;
    private int episodeId;

}
