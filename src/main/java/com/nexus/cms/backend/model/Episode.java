package com.nexus.cms.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "episodes")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Episode implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "subtitle")
    private String subtitle;

    @Column(name = "number")
    private Integer number;

    @Column(name = "description")
    private String description;

    @Column(name = "content")
    private String content;

    @Column(name = "notes")
    private String notes;

    @Column(name = "external_notes_url")
    private String externalNotesUrl;

    @Column(name = "state")
    private String state;

    @Column(name = "hidden")
    private Boolean hidden;

    @Column(name = "unlisted")
    private Boolean unlisted;

    @Column(name = "nsfw")
    private Boolean nsfw;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "updated_at")
    private Timestamp updatedAt;

    @Column(name = "published_at")
    private Timestamp publishedAt;

    @Column(name = "deleted_at")
    private Timestamp deletedAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "series_id")
    private Series series;

    @ManyToOne
    @JoinColumn(name = "album_art_id")
    private AlbumArt albumArt;

    @OneToMany(mappedBy = "parent")
    private List<EpisodeRelation> parents;

    @OneToMany(mappedBy = "child")
    private List<EpisodeRelation> children;

}
