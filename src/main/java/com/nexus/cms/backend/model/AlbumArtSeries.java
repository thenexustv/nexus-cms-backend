package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "album_art_series")
public class AlbumArtSeries {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private int albumArtId;
    private int seriesId;

}
