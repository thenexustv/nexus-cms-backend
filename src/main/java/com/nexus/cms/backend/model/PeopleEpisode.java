package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "people_episodes")
public class PeopleEpisode {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String role;
    private int episodeId;
    private int personId;

}
