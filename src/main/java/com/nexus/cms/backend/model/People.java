package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "people")
public class People {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String name;
    private String email;
    private String slug;
    private String webUrl;
    private String twitterUrl;
    private String avatarUrl;
    private String content;
    private Boolean hidden;
    private Timestamp createdAt;
    private Timestamp updatedAt;

}
