package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "people_series")
public class PeopleSeries {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private int seriesId;
    private int personId;

}
