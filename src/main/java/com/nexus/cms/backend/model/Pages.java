package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "pages")
public class Pages {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String name;
    private String slug;
    private String description;
    private String content;
    private byte hidden;
    private Timestamp createdAt;
    private Timestamp updatedAt;

}
