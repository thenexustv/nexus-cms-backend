package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "episodes_relations")
public class EpisodeRelation {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private String type;

    @ManyToOne
    @JoinColumn(name="episodeId")
    private Episode parent;

    @ManyToOne
    @JoinColumn(name="episodeRelatedId")
    private Episode child;
}
