package com.nexus.cms.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "album_art_views")
public class AlbumArtView {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String name;
    private String filename;
    private String path;
    private int width;
    private int height;
    private byte hidden;
    private int albumArtId;

}
