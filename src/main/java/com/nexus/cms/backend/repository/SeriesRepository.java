package com.nexus.cms.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexus.cms.backend.model.Series;

@Repository
public interface SeriesRepository extends JpaRepository<Series, Integer> {
    Series findBySlug(String slug);
}
