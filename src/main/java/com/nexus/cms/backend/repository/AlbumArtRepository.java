package com.nexus.cms.backend.repository;

import com.nexus.cms.backend.model.AlbumArt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumArtRepository extends JpaRepository<AlbumArt, Integer> {
}
