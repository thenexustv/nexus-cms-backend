package com.nexus.cms.backend.controller;

import com.nexus.cms.backend.model.People;
import com.nexus.cms.backend.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PeopleController {

    @Autowired
    private PeopleService peopleService;

    @RequestMapping(value = "/api/people")
    public List<People> getAllPeople() {
        return peopleService.getPeople();
    }

    @RequestMapping(value = "/api/people/paged")
    public Page<People> getAllPeople(Pageable pageable) {
        return peopleService.getPeople(pageable);
    }

    @RequestMapping(value = "/api/people/id/{id}")
    public People getPeopleById(@PathVariable Integer id) {
        return peopleService.getById(id);
    }

}
