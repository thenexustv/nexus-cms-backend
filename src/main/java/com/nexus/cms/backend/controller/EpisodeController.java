package com.nexus.cms.backend.controller;

import java.util.List;

import com.nexus.cms.backend.model.Episode;
import com.nexus.cms.backend.service.EpisodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class EpisodeController {

    @Autowired
    private EpisodeService episodeService;

    @RequestMapping(value = "/api/episodes")
    public List<Episode> getAllEpisodes() {
        return episodeService.getEpisodes();
    }

    @RequestMapping(value = "/api/episodes/paged")
    public Page<Episode> getAllEpisodes(Pageable pageable) {
        return episodeService.getEpisodes(pageable);
    }

    @RequestMapping(value = "/api/episodes/{id}")
    public Episode getEpisodeById(@PathVariable Integer id) {
        log.debug("episode id: {}", id);
        return episodeService.getEpisodeById(id);
    }

}
