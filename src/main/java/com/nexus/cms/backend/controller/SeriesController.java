package com.nexus.cms.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexus.cms.backend.model.Series;
import com.nexus.cms.backend.service.SeriesService;

@RestController
public class SeriesController {

    @Autowired
    private SeriesService seriesService;

    @RequestMapping(value = "/api/series")
    public List<Series> getAllSeries() {
        return seriesService.getSeries();
    }

    @RequestMapping(value = "/api/series/paged")
    public Page<Series> getAllSeries(Pageable pageable) {
        return seriesService.getSeries(pageable);
    }

    @RequestMapping(value = "/api/series/id/{id}")
    public Series getSeriesById(@PathVariable Integer id) {
        return seriesService.getById(id);
    }

    @RequestMapping(value = "/api/series/slug/{slug}")
    public Series getSeriesBySlug(@PathVariable String slug) {
        return seriesService.getBySlug(slug);
    }

}
