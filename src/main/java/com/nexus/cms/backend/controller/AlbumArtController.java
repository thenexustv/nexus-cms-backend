package com.nexus.cms.backend.controller;

import com.nexus.cms.backend.model.AlbumArt;
import com.nexus.cms.backend.service.AlbumArtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AlbumArtController {

    @Autowired
    private AlbumArtService albumArtService;

    @RequestMapping(value = "/api/album-art")
    public List<AlbumArt> getAllAlbumArt() {
        return albumArtService.getAlbumArt();
    }

    @RequestMapping(value = "/api/album-art/paged")
    public Page<AlbumArt> getPageAlbumArt(Pageable pageable) {
        return albumArtService.getAlbumArt(pageable);
    }


    @RequestMapping(value = "/api/album-art/id/{id}")
    public AlbumArt getAlbumArtById(@PathVariable Integer id) {
        return albumArtService.getById(id);
    }

}
