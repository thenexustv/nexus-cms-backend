# Nexus CMS Backend

The Nexus is a podcasting network. This is the backend to the Nexus CMS written in Spring Boot.

## Install

Install the Java 8 JDK and JRE, maven, and mysql.

## Build

Start up your mysql server. For the first run, import the schema found in `src/main/resources/schema/schema.sql`. Here's what works on a default mysql installation via Homebrew, `mysql -u root <schema.sql`. You might need to set or comment out the `spring.datasource.password` variable in `src/main/resources/aplication.properties`.

Then, to build the project run the following:

```shell
mvn clean install
```

## Run

```shell
mvn spring-boot:run
```